import firebase from 'firebase/app';
import Vue from 'vue'

import {
  fetchArticleWhere,
  fetchArticlesWhere,
  fetchArticlesPaginated,
  fetchProfile,
  inCollection,
  fetchMarkdown
} from './utils'


require('firebase/storage')
require('firebase/firestore')





const state = () => ({
  collections: {
    // where user information is stored
    profiles: 'profiles',
    // where articles are stored
    articles: 'blogPosts',
    // where article _documents_ are stored
    storage: 'blog/posts/'
  },

  // current page
  page: 0,
  // elements per page
  perPage: 10,
  // list of ids of fetched article
  pageIds: null,


  slugArticle: null,
  slugTextDoc: null,
  slugAuthors: null,

  // whether or not currently fetching
  fetching: false,

  // id: article of fetched articles
  articles: {},
  // id: markdown of fetched documents
  textdocs: {},
  // id: profile of fetched author profiles
  profiles: {}


})

const actions = {
  async validateSlug({state, getters, commit}, slug) {

    let articleId = inCollection({c: state.articles, k: 'slug', v: slug})
    if (articleId) {
      commit('set', {k: 'slugArticle', v: article})
      return true
    }
    commit('setFetching', true)
    let articles = await fetchArticlesWhere({
      key: 'slug', val: slug, comp: '==', limit: 1,
      articlesRef: getters.articlesRef
    })
    commit('setFetching', false)
    if (!articles || articles.length === 0) {
      return false
    }

    let article = articles[0]
    commit('setCollection', {c: 'articles', k: article.id, v: article})
    return true
  },


  async fetchSlugArticle({state, getters, commit, dispatch}, slug) {

    console.log(slug, state.articles)
    let articleId = inCollection({c: state.articles, k: 'slug', v: slug})
    console.log(articleId)
    let article   = articleId ? state.articles[articleId] : articleId
    console.log(article)
    if (!articleId) {
      commit('setFetching', true)
      let articles = await fetchArticlesWhere({
        key: 'slug', val: slug, comp: '==', limit: 1,
        articlesRef: getters.articlesRef
      })
      if (!articles || articles.length === 0) {
        commit('setFetching', false)
        return
      }
      article = articles[0]
      commit('setCollection', {c: 'articles', k: article.id, v: article})
    }
    commit('set', {k: 'slugArticle', v: article})

    dispatch('fetchSlugTextDoc')
    dispatch('fetchSlugAuthors')
  },
  async fetchSlugTextDoc({state, getters, commit}) {

    let article   = state.slugArticle
    console.log('fetchSlugTextDoc', article)
    let markdown  = state.textdocs[article.id]
    console.log('fetchSlugTextDoc', markdown)
    let alreadyHave = (markdown !== undefined)

    if (!alreadyHave) {
      commit('setFetching', true)
      let url  = await getters.storageRef.child(`${article.id}`).getDownloadURL()
      markdown = await fetchMarkdown(url)
      commit('setCollection', { c: 'textdocs', k: article.id, v: markdown })
      commit('setFetching', false)
    }

    commit('set', {k: 'slugTextDoc', v: markdown})
  },


  async fetchSlugAuthors({state, getters, commit}) {

    let article   = state.slugArticle
    console.log('fetchSlugTextDoc', article)
    let authors   = article.authors
    console.log('fetchSlugTextDoc', authors)
    let authorIds = Object.keys(authors)

    let alreadyHave = (authorIds!== undefined)
    let profiles    = alreadyHave ? authorIds.every(id=>state.profiles[id]) : null
    if (profiles) alreadyHave = profiles.every(p=>p!==undefined)

    if (!alreadyHave) {
      commit('setFetching', true)
      profiles = await Promise.all(authorIds.map(id=>fetchProfile({id, profilesRef:getters.profilesRef})))
      profiles.forEach(profile=> {
        commit('setCollection', {c: 'profiles', k: profile.id, v: profile })
      })
      commit('setFetching', false)
    }
    commit('set', {k: 'slugAuthors', v: profiles})
  },

  async fetchPage({state, getters, commit}, page=0) {

    commit('setFetching', true)
    commit('set', {k: 'page', v: page})

    let posts = await fetchArticlesPaginated({
      page: state.page,
      perPage: state.perPage,
      articlesRef: getters.articlesRef,
      orderBy: "timestamp"
    })

    posts.forEach((post)=>{
      commit('setCollection', { c: 'articles', k: post.id, v: post})
    })

    commit('set', {k: 'pageIds', v: posts.map(p=>p.id)})
    commit('setFetching', false)
  },

}

const mutations = {

  set(state, {k, v}) {
    state[k] = v
  },

  setFetching(state, bool) {
    state.fetching = bool
  },
  setSlug(state, {article, profiles, markdown}) {
    state.slugArticle = article
    state.slugTextDoc = markdown
    state.slugAuthors = profiles
  },
  setCollection(state, {c, k, v}) {
    Vue.set(state[c], k, v)
  }


}

const getters = {
  db: (state) => firebase.firestore(),
  sb: (state) => firebase.storage().ref(),
  profilesRef: (state, getters) => getters.db.collection(state.collections.profiles),
  articlesRef: (state, getters) => getters.db.collection(state.collections.articles),
  storageRef:  (state, getters) => getters.sb.child(state.collections.storage),
}




export default {
  namespaced: true,
  state, actions, mutations, getters
}
