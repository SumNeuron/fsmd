export const inCollection = ({c, k, v}) => {
  if (!c) return null
  let cKeys = Object.keys(c)
  for (let i = 0; i < cKeys.length; i++) {
    let ok = cKeys[i]
    if (c[ok][k] === value) {
      return ok
    }
  }
  return null
}


const readMarkdownBlob = (blob) => {
  return new Promise( (resolve, reject) => {
    let reader = new FileReader();
    reader.addEventListener('loadend', (e) => {
      let read = e.srcElement.result;
      resolve(read)
    });
    reader.readAsText(blob);
  })
}


export const fetchMarkdownBlob = async (url) => {
  let response = await fetch(new Request(url), {
    method: 'GET',
  })
  let blob = await response.blob()
  return blob
}

export const fetchMarkdown = async (url) => {
  try {
    let blob = await fetchMarkdownBlob(url)
    let text = await readMarkdownBlob(blob)
    return text
  }
  catch (error) {
    switch (error.code) {
      case 'storage/object-not-found':
        // File doesn't exist
        break;
      case 'storage/unauthorized':
        // User doesn't have permission to access the object
        break;
      case 'storage/canceled':
        // User canceled the upload
        break;
      case 'storage/unknown':
        // Unknown error occurred, inspect the server response
        break;
      }
  }
}



export const fetchProfile = async ({id, profilesRef}) => {
  let doc = await profilesRef.doc(id).get()
  return Object.assign({id}, doc.data())
}

export const fetchArticlesWhere = async ({
  key, val, comp="==",
  limit=1,
  articlesRef
}) => {
  let docsSnapshot = await articlesRef.where(key, comp, val)
                                      .limit(limit).get()
  if (docsSnapshot.docs.length) {
    let docs = docsSnapshot.docs.map(doc=>{
      return Object.assign({id: doc.id}, doc.data())
    })
    return docs
  }
  return null
}

export const fetchArticlesPaginated = async ({
  page, perPage, articlesRef,
  orderBy="timestamp"
}) => {
  let docsSnapshot = await articlesRef.limit(perPage)
                          .orderBy(orderBy)
                          .startAt(page*perPage)
                          .get()

  return docsSnapshot.docs.map((doc)=>{
    return Object.assign({id: doc.id}, doc.data())
  })
}


export const fetchArticleWhere = async ({
  key, val, comp="==",
  articlesRef,
  profilesRef,
  storageRef
}) => {

  let articles = await fetchArticlesWhere({key, val, comp, limit:1, articlesRef})
  if (!articles || articles.length === 0) return

  let article = articles[0]


  let url  = await storageRef.child(`${article.id}`).getDownloadURL()
  let markdown = fetchMarkdown(url)
  let authorIds = Object.keys(article.authors)
  let profiles  = Promise.all(authorIds.map(id=>fetchProfile({id, profilesRef})))


  // [profiles, markdown] = await Promise.all([profiles(), markdown])

  markdown = await markdown
  profiles = await profiles
  return {
    article,
    profiles,
    markdown
  }
}
