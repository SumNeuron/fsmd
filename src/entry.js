import Vue from 'vue';


import AsyncSlot from './components/AsyncSlot.vue'
import ReadingTime from './components/ReadingTime.vue'
import module from './modules/fsmd/index.js'

const components = {
  AsyncSlot,
  ReadingTime
}

function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  Object.keys(components).forEach(name => {
    Vue.component(name, components[name])
  });

}

const plugin = {
  install,
}

let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}


export { module }
// export { ...components }
export {
  AsyncSlot,
  ReadingTime
}
export const strict = false
export default components
